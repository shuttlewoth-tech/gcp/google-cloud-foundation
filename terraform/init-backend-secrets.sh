#!/usr/bin/env zsh

check_bw_unlocked(){
    if bw unlock --check > /dev/null 2>&1
    then
      echo "Bitwarden is unlocked"
    else
      echo "Bitwarden is locked"
      exit 1
    fi
}

PROJECT_ID="43661290"
STATE_NAME="default"
GITLAB_API_KEY_ID="366e5ee5-75eb-45d3-ac32-ad82012bf86c"

check_bw_unlocked

echo "Fetching GITLAB_ACCESS_TOKEN"
TF_VAR_GITLAB_ACCESS_TOKEN=$(bw get item ${GITLAB_API_KEY_ID} | jq -r '.fields[0] .value')
export TF_VAR_GITLAB_ACCESS_TOKEN

echo "Setting GITLAB_USER"
TF_VAR_GITLAB_USER="aurelian-shuttleworth"
export TF_VAR_GITLAB_USER

echo "Setting GITLAB_ADDRESS"
TF_VAR_GITLAB_ADDRESS="https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/${STATE_NAME}"
export TF_VAR_GITLAB_ADDRESS


terraform init \
    -backend-config="address=${TF_VAR_GITLAB_ADDRESS}" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/$PROJECT_ID/terraform/state/$STATE_NAME/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/$PROJECT_ID/terraform/state/$STATE_NAME/lock" \
    -backend-config="username=${TF_VAR_GITLAB_USER}" \
    -backend-config="password=${TF_VAR_GITLAB_ACCESS_TOKEN}" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"