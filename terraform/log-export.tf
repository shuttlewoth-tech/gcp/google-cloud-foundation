module "logsink-978534185875-logbucketsink" {
  source  = "terraform-google-modules/log-export/google"
  version = "~> 7.3.0"

  destination_uri      = module.shuttleworth-logging-destination.destination_uri
  log_sink_name        = "978534185875-logbucketsink"
  parent_resource_id   = var.org_id
  parent_resource_type = "organization"
  include_children     = true
}

module "shuttleworth-logging-destination" {
  source  = "terraform-google-modules/log-export/google//modules/logbucket"
  version = "~> 7.4.1"

  project_id               = module.logging-ta715-pb722.project_id
  name                     = "shuttleworth-logging"
  location                 = "global"
  retention_days           = 30
  log_sink_writer_identity = module.logsink-978534185875-logbucketsink.writer_identity
}
