module "logging-ta715-pb722" {
  source  = "terraform-google-modules/project-factory/google"
  version = "~> 12.0"

  name       = "logging"
  project_id = "logging-ta715-pb722"
  org_id     = var.org_id
  folder_id  = google_folder.common.name

  billing_account = var.billing_account
  activate_apis =  ["compute.googleapis.com", "secretmanager.googleapis.com"]
}

module "monitoring-dev-ta715-pb722" {
  source  = "terraform-google-modules/project-factory/google"
  version = "~> 12.0"

  name       = "monitoring-dev"
  project_id = "monitoring-dev-ta715-pb722"
  org_id     = var.org_id
  folder_id  = google_folder.common.name

  billing_account = var.billing_account
  activate_apis =  ["compute.googleapis.com", "secretmanager.googleapis.com"]
}

module "monitoring-nonprod-ta715-pb722" {
  source  = "terraform-google-modules/project-factory/google"
  version = "~> 12.0"

  name       = "monitoring-nonprod"
  project_id = "monitoring-nonprod-ta715-pb722"
  org_id     = var.org_id
  folder_id  = google_folder.common.name

  billing_account = var.billing_account
  activate_apis =  ["compute.googleapis.com", "secretmanager.googleapis.com"]
}

module "monitoring-prod-ta715-pb722" {
  source  = "terraform-google-modules/project-factory/google"
  version = "~> 12.0"

  name       = "monitoring-prod"
  project_id = "monitoring-prod-ta715-pb722"
  org_id     = var.org_id
  folder_id  = google_folder.common.name

  billing_account = var.billing_account
  activate_apis =  ["compute.googleapis.com", "secretmanager.googleapis.com"]
}

module "vpc-host-dev-ta715-pb722" {
  source  = "terraform-google-modules/project-factory/google"
  version = "~> 12.0"

  name       = "vpc-host-dev"
  project_id = "vpc-host-dev-ta715-pb722"
  org_id     = var.org_id
  folder_id  = google_folder.common.name

  enable_shared_vpc_host_project = true
  billing_account = var.billing_account
  activate_apis =  ["compute.googleapis.com", "secretmanager.googleapis.com"]
}

module "vpc-host-nonprod-ta715-pb722" {
  source  = "terraform-google-modules/project-factory/google"
  version = "~> 12.0"

  name       = "vpc-host-nonprod"
  project_id = "vpc-host-nonprod-ta715-pb722"
  org_id     = var.org_id
  folder_id  = google_folder.common.name

  enable_shared_vpc_host_project = true
  billing_account = var.billing_account
  activate_apis =  ["compute.googleapis.com", "secretmanager.googleapis.com"]
}

module "vpc-host-prod-ta715-pb722" {
  source  = "terraform-google-modules/project-factory/google"
  version = "~> 12.0"

  name       = "vpc-host-prod"
  project_id = "vpc-host-prod-ta715-pb722"
  org_id     = var.org_id
  folder_id  = google_folder.common.name

  enable_shared_vpc_host_project = true
  billing_account = var.billing_account
  activate_apis =  ["compute.googleapis.com", "secretmanager.googleapis.com"]
}