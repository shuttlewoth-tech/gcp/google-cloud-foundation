module "core" {
  source  = "terraform-google-modules/project-factory/google"
  version = "~> 12.0"

  name       = "core"
  project_id = "core-378317"
  org_id     = var.org_id
  folder_id  = google_folder.common.name

  billing_account = var.billing_account
  activate_apis =  ["compute.googleapis.com", "secretmanager.googleapis.com"]
}

resource "google_storage_bucket" "core_state_bucket" {
  name                     = "${module.core.project_id}-terraform-state"
  project                  = module.core.project_id
  location                 = local.default_bucket_location
  public_access_prevention = "enforced"

  versioning {
    enabled = true
  }
}

resource "google_storage_bucket_iam_binding" "core_state_bucket" {
  bucket   = google_storage_bucket.core_state_bucket.name
  role     = "roles/storage.objectAdmin"
  members = ["serviceAccount:${module.core.service_account_email}"]
}

module "core_token_binding" {
  source = "terraform-google-modules/iam/google//modules/service_accounts_iam"

  service_accounts = [module.core.service_account_email]
  project          = module.core.project_id
  mode             = "authoritative"
  bindings = {
    "roles/iam.serviceAccountTokenCreator" = [
      "group:organization-admins@shuttleworth.tech"
    ]
  }
}