module "organization-iam" {
  source  = "terraform-google-modules/iam/google//modules/organizations_iam"
  version = "~> 7.4"

  organizations = ["978534185875"]

  bindings = {
    
    "roles/billing.admin" = [
      "group:billing-admins@shuttleworth.tech",
    ]
    
    "roles/resourcemanager.organizationAdmin" = [
      "group:organization-admins@shuttleworth.tech",
    ]
    
  }
}


module "development-iam" {
  source  = "terraform-google-modules/iam/google//modules/folders_iam"
  version = "~> 7.4"

  folders = [google_folder.development.name]

  bindings = {
    
    "roles/compute.instanceAdmin.v1" = [
      "group:developers@shuttleworth.tech",
    ]
    
    "roles/container.admin" = [
      "group:developers@shuttleworth.tech",
    ]
    
  }
}


module "non-production-iam" {
  source  = "terraform-google-modules/iam/google//modules/folders_iam"
  version = "~> 7.4"

  folders = [google_folder.non-production.name]

  bindings = {
    
    "roles/compute.instanceAdmin.v1" = [
      "group:developers@shuttleworth.tech",
    ]
    
    "roles/container.admin" = [
      "group:developers@shuttleworth.tech",
    ]
    
  }
}
